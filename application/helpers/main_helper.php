<?php 

if ( ! function_exists('RenderPage'))
{
function RenderPage($page_to_display, $data=FALSE)
{
	$CI =& get_instance(); 
	
	if(!isset($data['page_title']))
		$data['page_title'] = 'FORUM';
	$CI->load->view('header', $data);
	if(!$data)
		$CI->load->view($page_to_display);
	else
		$CI->load->view($page_to_display, $data);
	$CI->load->view('footer');
}
}

if ( ! function_exists('CheckLogin'))
{
	function CheckLogin()
	{
		$CI =& get_instance();
		
		$priv=array();
		if(!$CI->session->userdata('logged'))
		{
			return false;
		}
		
		$priv['rank'] = $CI->session->userdata('user_rank');
		
		if ($priv['rank']=='2')
			$priv['mod'] = $CI->forum_model->User_CheckMod($CI->session->userdata('user_id'));
				
		return $priv;
		
	}
}
