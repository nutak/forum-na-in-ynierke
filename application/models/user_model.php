<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('email');
		$this->load->helper('cookie');
	}
	
	function Login($data)
	{
		$this->db->select('user_id, login, password, email, rank');
		$this->db->where('login', $data['login']);
		$query = $this->db->get('user');
		
		if ($query->num_rows()==0)
			return false;
		
		$result = $query->result();
		
		if (!password_verify($data['password'] ,$result['password']));
			return false;
		
		$this->session->set_userdata(array(
				'user_id'	=>	$result['user_id'],
				'user_login'=>	$result['login'],
				'user_email'=>	$result['email'],
				'user_rank'	=>	$result['rank'],
				'logged'	=>	TRUE	
		));
			
		if ($data['remember_me'])
		{
			set_cookie('id',$result['user_id'],0,base_url(),'','user_',TRUE);
			set_cookie('token',$result['user_id'],0,base_url(),'','user_',TRUE);
		}
		
		return true;
	}
	
	function Register($data)
	{
		$db_data=array(
				'login'		=> $data['login'],
				'password'  => $Hasher($data['password']),
				'email'		=> $data['email'] 
		);
		$this->db->insert($db_data);
		
		
		return true;
		
	}
	
	function GetUserList()
	{
		//All registred user list
	}
	
	function GetUserInfo()
	{
		//all user details
	}
	
	function UpdateUserInfo()
	{
		
	}
	
}
