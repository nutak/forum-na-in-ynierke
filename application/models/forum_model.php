<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class forum_model extends CI_Model
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('email');
		$this->load->helper('cookie');
	}
	
	function Topic_Count($priv, $id)
	{
		$this->db->where('topic_id', $id);
		$this->db->where('post_id', 1);
		$this->db->where('rank_view <=', $priv['rank']);
		return $this->db->count_all_results('topic');
	}
	
	function Topic_CountInSection($id)
	{
		$this->db->where('section_id', $id);
		return $this->db->count_all_results('topic');
	}
		
	function LoadSection($privileges ,$id=NULL)
	{
				
		$i=0;
		$output_data=array();
		//dane wyswietlanego dzialu
		if($id!=NULL)
		{
			$this->db->where('section_id', $id);
			$query = $this->db->get('section');
			$output_data = $query->result();
			
			if ($output_data['rank_limit']>$privileges['rank'])
				return false;
		}
		
		//dzialy znajdujace sie wew. danego dzialu
		$this->db->where('parent_section_id', $id);
		$this->db->where('rank_limit <=', $privileges['rank']);
		$this->db->order_by('sections_order');
		$query = $this->db->get('section');
		if ($query->num_rows()>0)
		{
			if ($id!=NULL)
				array_push($output_data, 'sections');
			foreach ($query->result() as $data)
			{
				if ($id==NULL)
					array_push($output_data, 'sections');
				array_push($output_data, $data);
				
				//to dziala tylko dla glownego dzialu - glownej strony forum
				if ($id==NULL)
				{
					
					$this->db->where('parent_section_id', $data->section_id);
					$this->db->where('rank_limit <=', $privileges['rank']);
					$this->db->order_by('sections_order');
					$query_b = $this->db->get('section');
					if ($query_b->num_rows()>0)
					{
						array_push($output_data, 'sections_inside');
						foreach ($query_b->result() as $data_b)
						{
							$i++;
							array_push($output_data, $data_b);
						}
					}
					
					//tematy dzialow wewnatrz ^tych^ dzialow
					$this->db->where('section_id', $data->section_id);
					$this->db->where('rank_view <=', $privileges['rank']);
					$this->db->where('post_id',1);
					if ($privileges['rank']>1)
					{
						if ($privileges['rank']==2)
						{
							foreach ($privileges['moderation'] as $mod)
							{
								if($mod->section_id==$data->section_id)
									$this->db->or_where('rank_view', NULL);
							}
						}
						else 
							$this->db->or_where('rank_view', NULL);
					}
					$query_b = $this->db->get('topic');
					if ($query_b->num_rows()>0)
					{	
						array_push($output_data, 'topics_inside');
						foreach ($query_b->result() as $data_b)
						{
							$i++;
							array_push($output_data, $data_b);
						}
					}
				}
			}
		}
		
		//tematy znajdujace sie w podanym dziale
		$this->db->where('section_id', $id);
		$this->db->where('rank_view <=', $privileges['rank']);
		$this->db->where('post_id',1);
		if ($privileges['rank']>1)
		{
			if ($privileges['rank']==2)
			{
				foreach ($privileges['moderation'] as $mod)
				{
					if($mod->section_id==$data->section_id)
						$this->db->or_where('rank_view', NULL);
				}
			}
			else
				$this->db->or_where('rank_view', NULL);
		}
		$query = $this->db->get('topic');
		if ($query->num_rows()>0)
		{
			array_push($output_data, 'topics');
			foreach ($query->result() as $data)
			{
				array_push($output_data, $data);
			}
		}
		//else
			//return false;
		
		
		return $output_data;
	}
	
	function LoadTopic($privileges,$id,$limit=10,$start=0,$count=false)
	{
		$output_data=array();
		
		$this->db->where('topic_id', $id);
		$this->db->where('rank_view <=', $privileges['rank']);
		if (!$count)
			$this->db->limit($limit, $start);
		if ($privileges['rank']>1)
		{
			if ($privileges['rank']==2)
			{
				foreach ($privileges['mod'] as $mod)
				{
					if($mod->section_id==$data->section_id)
						$this->db->or_where('rank_view', NULL);
				}
			}
			else
				$this->db->or_where('rank_view', NULL);
		}
		if ($count)
			return $this->db->count_all_results('topic');
		$query = $this->db->get('topic');
		if ($query->num_rows()>0)
		{
			array_push($output_data, 'topic');
			foreach ($query->result() as $data)
			{
				array_push($output_data, $data);
			}
		}
		
		return $output_data;
	}
	
	
}