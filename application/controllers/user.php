<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends CI_Controller {

	/**
	 * Temat pracy inżynierskiej - 'Forum internetowe z wykorzystaniem biblioteki jQuery'
	 * Januszę 
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'url_title_pl'));
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->model('forum_model');
		$this->load->database();
	}
	
	public function index()
	{
		//USERPANEL
	}
	
	public function Register()
	{
		$last_page = $this->input->get('p');
		
		if (CheckLogin())
		{
			redirect(site_url($last_page));
			return false;
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('login', 'Nazwa Użytkownika', 'trim|required|min_length[4]|xss_clean');
		$this->form_validation->set_rules('pass', 'Hasło', 'trim|required|min_length[6]|xss_clean');
		$this->form_validation->set_rules('pass_conf', 'Potwierdzenie Hasła', 'trim|required|matches[pass]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[user.email]');
		$validation_error=TRUE;
		
		if ($this->form_validation->run())
		{
			$data=array(
					'login'=> $this->input->post('login'),
					'password'=> $this->input->post('pass'),
					'password2'=>$this->input->post('pass_conf'),
					'email'=>$this->input->post('email')
			);
			$result = $this->forum_model->Register($data);
			if ($result)
			{
				$validation_error=FALSE;
				redirect(site_url($last_page));
			}
		}
		
		if ($validation_error)
			RenderPage('register', $data);
	}
	
	public function Logout()
	{
		$this->forum_model->Logout();
	}
	
	public function Login()
	{
		$last_page = $this->input->get('p');
		
		if (CheckLogin())
		{
			redirect(site_url($last_page));
			return false;
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('login', 'Nazwa Użytkownika', 'trim|required|min_length[4]|xss_clean');
		$this->form_validation->set_rules('pass', 'Hasło', 'trim|required|min_length[4]|xss_clean');
		$this->form_validation->set_rules('rem', 'Zapamiętaj mnie', 'integer');
		
		
		if($this->form_validation->run())
		{
			$data=array(
					'login'=> $this->input->post('login'),
					'password'=> $this->input->post('pass'),
					'remember_me'=>$this->input->post('rem')						
			);
			$result = $this->forum_model->Login($data);
			if ($result)
				redirect(site_url($last_page));
			
		}

		if ($validation_error)
			RenderPage('login', $data);
	}
	
	public function AllUsers()
	{
		//registred user list
	}
	
	public function Info($user_id)
	{
		//user details
	}
}