<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class forum extends CI_Controller {

	/**
	 * Temat pracy inżynierskiej - 'Forum internetowe z wykorzystaniem biblioteki jQuery'
	 * Januszę 
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'url_title_pl'));
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->model('forum_model');
		$this->load->database();
	}
	
	public function index()
	{
		
		$privileges = CheckLogin();
		if (!$privileges)
			$privileges['rank'] = 0;
		
		$data['db_result'] = $this->forum_model->LoadSection($privileges);
		RenderPage('main', $data);
		
	}
	
	public function Section($section)
	{
		$privileges = CheckLogin();
		if (!$privileges)
			$privileges['rank'] = 0;
		
		$pos = strpos($section, '_', 0);
		$id = substr($section, 0, $pos);
		
		
		//---------- do paginacji
		$segment=4;
		$config['base_url'] = base_url() . '/index.php/forum/section/' . $section;
		$config['total_rows'] = $this->forum_model->Topic_Count($privileges, $id);
		$config['per_page'] = 10; //i dodac jeszcze cookies do tego
		$config["uri_segment"] = $segment;
		
		$this->pagination->initialize($config);
		
		$page_num = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
		$dane["db_result"] = $this->forum_model->LoadSection($privileges,$id,$config["per_page"], $page_num);
		$dane["links"] = $this->pagination->create_links();
		//------------
		
		//$dane['db_wynik'] = $this->forum_model->WczytajDzial($upr ,$id);
		
		RenderPage('section', $dane);
	}
	
	public function Topic($topic)
	{
		$privileges = CheckLogin();
		if (!$privileges)
			$privileges['rank'] = 0;
		
		$pos = strpos($topic, '_', 0);
		$id = substr($topic, 0, $pos);
		//---------- do paginacji
		
		$segment=4;
		$config['base_url'] = site_url('/forum/topic/' . $topic);
		$config['total_rows'] = $this->forum_model->LoadTopic($privileges,$id,null,null,true);
		$config['per_page'] = 10; //i dodac jeszcze cookies do tego
		$config["uri_segment"] = $segment;
		
		$this->pagination->initialize($config);
		
		$page_num = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
		$data["db_result"] = $this->forum_model->LoadTopic($privileges,$id,$config["per_page"], $page_num);
		$data["links"] = $this->pagination->create_links();
		//------------
		
		
		//$dane['db_wynik'] = $this->forum_model->WczytajTemat($id);
		RenderPage('topic', $data);
		
	}
	
	
	
	/*
	public function Publikuj($dane)
	{
		if(!SprawdzLogowanie())
		{
			$this->Logowanie();
			return FALSE;
		}
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('tytul', 'Tytuł', 'trim|required|min_length[4]|xss_clean');
		
		$blad_walidacji=true;
		$dane['blad']=false;
		
		
		switch ($dane['typ'])
		{
			case 'temat':
				
				break;
			case 'post':
				
				break;
		}
		
		
	}
	
*/
	
	
}

