-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 01 Lut 2015, 17:05
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `baza2`
--
CREATE DATABASE IF NOT EXISTS `baza2` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `baza2`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `moderation`
--

CREATE TABLE IF NOT EXISTS `moderation` (
  `mod_id` int(7) NOT NULL AUTO_INCREMENT,
  `user_id` int(7) NOT NULL,
  `section_id` int(7) NOT NULL,
  PRIMARY KEY (`mod_id`),
  KEY `ID_uzyt` (`user_id`),
  KEY `ID_dzial` (`section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `section_id` int(7) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `rank_limit` int(3) DEFAULT NULL,
  `parent_section_id` int(7) DEFAULT NULL,
  `sections_order` int(5) DEFAULT NULL,
  `make_topics` tinyint(1) NOT NULL,
  PRIMARY KEY (`section_id`),
  KEY `naddzial_ID` (`parent_section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tag`
--

CREATE TABLE IF NOT EXISTS `tag` (
  `tag_id` int(7) NOT NULL AUTO_INCREMENT,
  `tag` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tagger`
--

CREATE TABLE IF NOT EXISTS `tagger` (
  `tagger_id` int(7) NOT NULL AUTO_INCREMENT,
  `topic_id` int(7) NOT NULL,
  `tag_id` int(7) NOT NULL,
  PRIMARY KEY (`tagger_id`),
  KEY `ID_temat` (`topic_id`),
  KEY `ID_tag` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `topic`
--

CREATE TABLE IF NOT EXISTS `topic` (
  `topic_id` int(7) NOT NULL,
  `post_id` int(7) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `picture` varchar(256) DEFAULT NULL,
  `decription` varchar(256) DEFAULT NULL,
  `text` longtext NOT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_mod` datetime DEFAULT NULL,
  `glued` int(2) DEFAULT NULL,
  `rank_view` int(3) DEFAULT NULL,
  `rank_mod` int(3) DEFAULT NULL,
  `section_id` int(7) DEFAULT NULL,
  `user_id` int(7) NOT NULL,
  `mod_by` int(7) DEFAULT NULL,
  PRIMARY KEY (`topic_id`,`post_id`),
  KEY `ID_dzial` (`section_id`),
  KEY `ID_uzyt` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `password` varchar(80) NOT NULL,
  `reg_date` timestamp NULL DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `rank` int(3) DEFAULT NULL,
  `ban` tinyint(1) DEFAULT NULL,
  `avatar_link` varchar(255) DEFAULT NULL,
  `name` varchar(120) DEFAULT NULL,
  `city` varchar(120) DEFAULT NULL,
  `age` date DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `moderation`
--
ALTER TABLE `moderation`
  ADD CONSTRAINT `moderation_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `moderation_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `section` (`section_id`);

--
-- Ograniczenia dla tabeli `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `section_ibfk_1` FOREIGN KEY (`parent_section_id`) REFERENCES `section` (`section_id`);

--
-- Ograniczenia dla tabeli `tagger`
--
ALTER TABLE `tagger`
  ADD CONSTRAINT `tagger_ibfk_1` FOREIGN KEY (`topic_id`) REFERENCES `topic` (`topic_id`),
  ADD CONSTRAINT `tagger_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`tag_id`);

--
-- Ograniczenia dla tabeli `topic`
--
ALTER TABLE `topic`
  ADD CONSTRAINT `topic_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`section_id`),
  ADD CONSTRAINT `topic_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
